/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sources;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author matheus
 */
public class xadrez {

    public Map<String, Integer> count(ArrayList<ArrayList<Integer>> board, Map<String, Integer> data) {
        //contadores para cada peca
        int peao = 0, torre = 0, cavalo = 0, bispo = 0, rei = 0, rainha = 0;

        Map<String, Integer> res = new HashMap<>();

        //percorrendo a entradas das pecas e armazenando os valores de cada peca em seus respectivos                                                                                                           
        //identificadores
        for (Map.Entry<String, Integer> entry : data.entrySet()) {
            if (null != entry.getKey()) {
                switch (entry.getKey()) {
                    case "peao":
                        peao = entry.getValue();
                        break;
                    case "torre":
                        torre = entry.getValue();
                        break;
                    case "cavalo":
                        cavalo = entry.getValue();
                        break;
                    case "bispo":
                        bispo = entry.getValue();
                        break;
                    case "rei":
                        rei = entry.getValue();
                        break;
                    case "rainha":
                        rainha = entry.getValue();
                        break;
                    default:
                        break;
                }
            }
        }
        
        //contadores para cada peca
        int peao_c = 0, torre_c = 0, cavalo_c = 0, bispo_c = 0, rei_c = 0, rainha_c = 0;
        
        //transformando o restado da porta logica OU exclusivo(XOR) -> 1: TRUE e 0:FALSE ; caso o valor seja 1,incrementa o contador                                                                           
        //(mais informacoes no metodo comparator)
        for(ArrayList<Integer> a : board){
            for(Integer i : a){
                peao_c += comparator(i, peao);
                torre_c += comparator(i, torre);
                cavalo_c += comparator(i, cavalo);
                bispo_c += comparator(i, bispo);
                rei_c += comparator(i, rei);
                rainha_c += comparator(i, rainha);
            }
        }

        //armazendo as respostas
        res.put("peao", peao_c);
        res.put("cavalo", cavalo_c);
        res.put("bispo", bispo_c);
        res.put("torre", torre_c);
        res.put("rei", rei_c);
        res.put("rainha", rainha_c);
        
        return res;
    }
    
    /*                                                                                                                                                                                                         
        utilizando a porta logica: ou exclusivo(XOR) para comparar dois inteiros,                                                                                                                              
        caso eles sejam iguais a comparacao retornaria 0 em todos os bits, logo o compilador transforma esse 0 em FALSE                                                                                        
        caso os numeros sejam diferentes o resultado seria 1 ou TRUE, eu utilizei o `== 0 e o Boolean.compare(c,false)` para inverter o resultado transformando                                                                                
        1 : caso iguais e 0: caso diferentes, logo a porta esta sendo usada como um comparador e um contador ao mesmo tempo.                                                                                   
    */
    private int comparator(int a , int b){
        
        Boolean c = (a ^ b) == 0;
        
        return Boolean.compare(c, false);
    }

}
